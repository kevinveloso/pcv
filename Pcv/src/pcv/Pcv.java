/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pcv;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Richelieu Costa 11328634 e Kevin Veloso 11318626
 */
public class Pcv {
    static int[][] a;
    public static long tBusca;
    /**
     * @param args
     *            the command line arguments
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
        Scanner input = null;
        try {
            input = new Scanner(new File("pcv01.txt"));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int rows = 0;
        int columns = 0;

        rows = columns = input.nextInt();

        a = new int[rows][columns];

        for (int i = 0; i < rows; ++i) {

            for (int j = 0; j < columns; ++j) {

                if (input.hasNextInt()) {
                    a[i][j] = input.nextInt();
                }

            }
        }

        input.close();
        
        Long c = new Long(0);


        for (int i = 2; i <= a[0].length; i++) {
            c=Long.sum(c, (long) Math.pow(2, i));
        }
        
        int numElementos = 0;
        
        for (int aux = 0; aux <= a[0].length; aux++) {

            if (((c >> aux) & 0x01) == 1) {
                numElementos++;
            }

        }

      
        Tupla pega = g(1, c, numElementos);
        pega.caminho.add(1);
        System.out.println("Menor custo: "+pega.custo);
        System.out.println("Circuito: "+pega.caminho);

    }

    public static Tupla g(int i, Long S, int numElementos) {
        
        Tupla custosEcaminhos[] = new Tupla[numElementos];

        Tupla tupla_aux;
        if (numElementos == 0) {
            
            return new Tupla(new Long(a[i - 1][0]), i);
            
        } else {
             int index_aux = 0;
             
                for (int aux = 1; aux <= a[0].length; aux++) { 

                    if (((S >> aux) & 0x01) == 1) {
                        
                        tupla_aux = g(aux, (S - (long)Math.pow(2, aux)), (numElementos - 1));
                        custosEcaminhos[index_aux]=new Tupla(Long.sum( a[i-1][aux-1], tupla_aux.custo),tupla_aux.caminho);
                        custosEcaminhos[index_aux].caminho.add(0,i);
                        index_aux++;

                       
                    }

                }
                

        }
        return getMin(custosEcaminhos);
    }

    public static Tupla getMin(Tupla[] array) {
        
        Tupla min = new Tupla(array[0].custo,array[0].caminho);

        for (Tupla elemento : array) {
            

           if (elemento.custo.longValue() < min.custo.longValue()) {
               
                min.caminho = elemento.caminho;
                min.custo = elemento.custo;
                
            }
           
        }

        return min;
    }

}